library(tidyverse)
library(nycflights13)

seq(1, 10)
seq(1, 15, length.out=3)
(seq(10))

#Practice
# my_variable <- 10
# my_varıable
# -> i & ı are different characters

# library(tidyverse)
#
# ggplot(data = mpg) +
#   geom_point(mapping = aes(x = displ, y = hwy))
#
# fliter(mpg, cyl = 8)
# filter(diamond, carat > 3)

ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy))

filter(mpg, cyl == 8)
filter(diamonds, carat > 3)

str(flights)
nrow(filter(flights, month == 1, day == 1))

1/2 == 0.5 # FALSE
near(1/2, 0.5) # TRUE

filter(flights, month == 11 | month == 12)
filter(flights, month %in% c("2", "4", "9"))

filter(flights, arr_delay < 120, dep_delay < 120)
filter(flights, !(arr_delay > 120 | dep_delay > 120))

x <- NA
is.na(x)

df <- tibble(x = c(1, 2, 3, NA, 5))
filter(df, df >= 3)
filter(df, is.na(df) | df >= 3)

# exercises 5.2.4
# 1.
a1 <- filter(flights, arr_delay >= 120)
min(a1$arr_delay)
a2 <- filter(flights, dest == 'IAH' | dest == 'HOU') %>% select(year, month, day, carrier, origin, dest)
unique(a2$dest)
packageDescription("nycflights13")
airports
planes
weather
flights
airlines
(a3 <- filter(flights, carrier %in% c("AA", "DL", "UA")))
unique(a3$carrier)
a4 <- filter(flights, month %in% c(7, 8, 9)) %>% select(year, month, day, carrier, origin, dest)
unique(a4$month)
(a5 <- filter(flights, arr_delay > 120, dep_delay <= 0))
min(a5$arr_delay)
max(a5$dep_delay)
(a6 <- filter(flights, dep_delay > 60, dep_delay - arr_delay > 30))
max(a6$arr_delay)
min(a6$dep_delay)
(a7 <- filter(flights, dep_time %% 2400 <= 600))
range(a7$dep_time)
# 2.
# between() returns TRUE or FALSE for each value in the passed vector that is or isn't between passed boundaries
(a8 <- filter(flights, between(dep_time, 0, 600)))
range(a7$dep_time)
# 3.
(a9 <- filter(flights, is.na(dep_time)))
# 8255 flights are missing departure time
# dep_delay, arr_time, arr_delay are also missing
# flights were cancelled?
# 4.
# NA combined with any other value using any operation that depends on what NA exactly is result in NA
NA ^ 0  # always 1 irrespective of what NA is
NA | TRUE # always TRUE irrespective of what NA is
FALSE & NA # always FALSE irrespective of what NA is
NA * 1 # NA
NA * 0  # should be 0 according to above logic but is NA

# exercises 5.3.1
# 1.
arrange(flights, desc(dep_delay))
a <- tibble(x = c(1,2,3,NA,8,5,NA,4,11,NA,7))
b <- tibble(x = c(TRUE, FALSE, TRUE))
arrange(b, x)
is.na(a)
arrange(a, desc(is.na(x)), x)
arrange(flights, desc(is.na(dep_time)), dep_time)
# 2.
arrange(flights, desc(dep_delay))[, c('origin', 'dest', 'flight', 'carrier', 'dep_delay')] # most delayed flights
arrange(flights, dep_time) # flights that left earliest
arrange(arrange(flights, desc(dep_delay)), dep_time)
arrange(arrange(flights, dep_time), desc(dep_delay))

# 3. 
arrange(flights, air_time)[,c('tailnum', 'air_time')]

# 4.
na.omit(arrange(flights, distance)[,c('tailnum', 'distance')])
arrange(flights, desc(distance))[,c('tailnum', 'distance')]

select(flights, year, tailnum)
select(flights, year:day)
select(flights, -(year:day))
select(flights, starts_with(('ye')))
select(flights, ends_with('ar'))
select(flights, contains('ea'))

rename(flights, tail_number=tailnum)[,'tail_number']
help(everything)
select(flights, time_hour, air_time, everything())

# exercises 5.4.1
# 1.
flights
select(flights, dep_time, dep_delay, arr_time, arr_delay)
select(flights, 'dep_time', 'dep_delay', 'arr_time', 'arr_delay')
select(flights, 4, 5, 6, 9)
select(flights, starts_with('dep'), starts_with('arr'))
select(flights, ends_with('time'), ends_with('delay'), -starts_with('sched'), -starts_with('air'))
select(flights, dep_time:arr_delay, -sched_dep_time, -sched_arr_time)
select(flights, c('dep_time', 'dep_delay', 'arr_time', 'arr_delay'))
select(flights, matches("^(dep|arr)_(time|delay)$"))

# 2.
select(flights, dep_time, dep_time)
# the column is selected only once

# 3.
names <- c('dep_time', 'dep_delay')
select(flights, one_of(names))
vars <- c("year", "month", "day", "dep_delay", "arr_delay")
select(flights, one_of(vars))
select(flights, vars)
select(flights, year, month, day, dep_delay, arr_delay)
# I swear to god I will never use one_of, looks useless 

# 4.
select(flights, contains('TIME'))
# why is contains case-insensitive lmao is this 1990
select(flights, contains('TIME', ignore.case=FALSE))

flights_sml <- select(flights, year:day,
                      ends_with('delay'),
                      distance, air_time)
mutate(flights_sml,
       gain = dep_delay - arr_delay,
       speed = distance / air_time * 60,
       hours = air_time / 60,
       gain_per_hour = gain / hours)

transmute(flights_sml,
       gain = dep_delay - arr_delay,
       speed = distance / air_time * 60)
transmute(flights, dep_time,
          hour = dep_time %/% 100,
          minute = dep_time %% 100)       
(x <- 1:10)
lag(x)
lead(lead(x))
cumsum(x)
?min_rank
x <- c(5, 1, 3, 2, 2, NA)
min_rank(x)
row_number(x)
dense_rank(x)

# exercises 5.5.2
select(flights, tailnum, dep_time, sched_dep_time)
# select() unnecessary if transmute() us used
# midnight is 2400, not handled below
(mutate(flights,
       dt_hour = dep_time %/% 100, 
       dt_minute = dep_time %% 100,
       min_since_midnight = dt_hour * 60 + dt_minute) 
  %>% 
    select(tailnum, dep_time, dt_hour, dt_minute, min_since_midnight))
(mutate(flights,
        sdt_hour = sched_dep_time %/% 100, 
        sdt_minute = sched_dep_time %% 100,
        min_since_midnight = sdt_hour * 60 + sdt_minute) 
  %>% 
    select(tailnum, sched_dep_time, sdt_hour, sdt_minute, min_since_midnight))

# 2.
to_minutes <- function(x) {
  (x %/% 100 * 60 + x %% 100)
}
# use the above function on dep_time and arr_time


# 3.
res <- (mutate(flights, 
       dep_delay2 = dep_time - sched_dep_time,
       dep_delay_error = dep_delay - dep_delay2) 
  %>% select(dep_time, sched_dep_time, dep_delay, dep_delay2, dep_delay_error))
range(res$dep_delay_error, na.rm=TRUE)
# the dep_delay and dep_delay2 should be equal but sometimes there's a difference of (-600)-2360.

# 4.
delayed = arrange(flights, desc(dep_delay))[1:10,]
str(delayed)
delayed$dep_delay
row_number(delayed$dep_delay)
mutate(delayed,
       rank = row_number(-dep_delay)) %>% select(rank, everything())

# 5.
1:3
1:10
1:3+1:9
c(1,2,3)+c(1,2,3,4,5,6,7,8,9)
# c(1,2,3) is being added to RHS vector from left-to-right
# c(1,2,3) + c(1,2,3), c(1,2,3) + c(4,5,6), c(1,2,3)+c(7,8,9)
# ans: 2  4  6  5  7  9  8 10 12
f <- select(flights, 1:13)
(ff <- colnames(f))
(fff = 1:length(ff))
data.frame(fg, fff)
g <- select(flights, 1:3+1:10)
g
# c(1,2,3) + c(1,2,3), c(1,2,3) + c(4,5,6), c(1,2,3)+c(7,8,9), c(1) + c(10)
# selects cols: 2,4,6,5,7,9,8,10,12,11

# 6.
x <- 45
cos(x)
sin(x)
tan(x)
acos(x)
asin(x)
atan(x)
y <- 90
atan2(y, x)

summarise(flights, delay = mean(dep_delay, na.rm=TRUE))
summarise(group_by(flights, year, month), delay = mean(dep_delay, na.rm=TRUE))

unique(flights$dest)
by_dest <- group_by(flights, dest)
delay <- summarise(by_dest,
                   count = n(),
                   dist = mean(distance, na.rm=TRUE),
                   delay = mean(arr_delay, na.rm=TRUE))
delay <- filter(delay, count > 20, dest != 'HNL')

ggplot(delay, aes(x=dist, y=delay)) +
  geom_point(aes(size=count), alpha=0.3) +
  geom_smooth(se=FALSE)

delays <- (flights %>% group_by(dest) 
                  %>% summarise(count = n(),
                                dist = mean(distance, na.rm=TRUE),
                                avg_delay = mean(arr_delay, na.rm=TRUE))
                  %>% filter(count > 20, dest != 'HNL'))
ggplot(delays, aes(x=dist, y=avg_delay)) +
  geom_point(aes(size=count), alpha=0.3) +
  geom_smooth(se=FALSE)

d <- tibble(x=c(1,2,3,4,5,6,7,8,9,10))
d
filter(d, !(x %% 2 == 0), !(x %% 3 == 0))

not_cancelled <- filter(flights, !is.na(dep_time), !is.na(arr_time))
delays <- summarise(group_by(not_cancelled, tailnum), 
                    avg_delay=mean(arr_delay),
                    count = n())
ggplot(delays, aes(x=count, y=avg_delay)) +
  geom_point()
ggplot(filter(delays, count > 50), aes(x=count, y=avg_delay)) +
  geom_point()

library('Lahman')
batting <- as_tibble(Lahman::Batting)
batters <- batting %>%
            group_by(playerID) %>%
            summarise(
              ba = sum(H, na.rm=TRUE) / sum(AB, na.rm=TRUE),
              ab = sum(AB, na.rm=TRUE)
            )
batters %>%
  filter(ab > 1000) %>%
  ggplot(aes(x=ab, y=ba)) +
  geom_point() +
  geom_smooth(se=FALSE)

# why is distance to some destinations more variable than others
flights %>% 
  group_by(dest) %>% 
  summarise(variance = sd(distance)) %>%
  arrange(desc(variance))

# When do the first and last flights leave each day?
not_cancelled %>%
  group_by(year, month, day) %>%
  summarise(first = min(dep_time),
            last = max(dep_time))
range(51)
5 %in% range(5)
not_cancelled %>% 
  group_by(year, month, day) %>% 
  mutate(r = min_rank(desc(dep_time)))
?distinct
?unique
?sample
df <- tibble(
  x = sample(10, 100, rep = TRUE),
  y = sample(10, 100, rep = TRUE)
)
df
nrow(df)
nrow(distinct(df))
nrow(distinct(df, x, y))
(distinct(df, x, .keep_all=TRUE))
?n_distinct

# Which destinations have the most carriers?
not_cancelled %>%
  group_by(dest) %>%
  summarise(n_carriers=n_distinct(carrier)) %>%
  arrange(desc(n_carriers))

# How many flights left before 5am? (these usually indicate delayed
# flights from the previous day)
?length
early_flights <- (not_cancelled %>%
  group_by(year, month, day) %>%
  summarise(n_early=sum(dep_time < 500)))
sum(early_flights$n_early)

# What proportion of flights are delayed by more than an hour?
nrow(filter(flights, dep_delay > 60)) / nrow(flights)
nrow(filter(flights, dep_delay > 60)) / nrow(filter(flights, dep_delay > 0))

not_cancelled %>%
  group_by(year, month, day) %>%
  summarise(delay_60_plus=sum(dep_delay>60)/n())
not_cancelled %>%
  group_by(year, month, day) %>%
  summarise(delay_60_plus=mean(dep_delay > 60))
not_cancelled %>%
  group_by(year, month, day) %>%
  summarise(delay_60_plus=mean(arr_delay > 60))

# exercise 5.6.7

install.packages('devtools')
library(devtools)
if (packageVersion("devtools") < "1.9.1") {
  message("Please upgrade devtools")
}
devtools::install_deps()
