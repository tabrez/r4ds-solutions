library(tidyverse)
library(nycflights13)

iris
iris_tibble <- as_tibble(iris)
iris_tibble
iris <- as.data.frame(iris_tibble)
iris

flights %>%
    print(n=15, width=Inf)
## set global defaults for priting tibbles:
## options(tibble.print_max = n, tibble.print_min = m)
## To always print all rows: options(tibble.print_min = Inf)
## To always print all columns: options(tibble.width = Inf)

package?tibble
## four important methods: print
## [, [[: extract by name or position
## $: extracts variables/columns by name only

##flights %>% View()

## Subsetting
df <- tibble(
  x = runif(5),
  y = rnorm(5)
)
df$x
df[['x']]
df[[1]]

df['x']

df %>%
    .$x

df %>%
    .[[1]]

## exercises 10.5

## 1.
## tibbles mention that it's a tibble when they're printed. rows have names,
## columns have types.
mtcars
as_tibble(mtcars)

## 2.
df <- data.frame(abc=1, xyz='a')
df
df$x ## partially matched with `xyz` column and returned it as a vector
df[, 'xyz'] ## values in column `xyz` returned as a vector
df[, c('abc', 'xyz')] ## returns both columns as a data.frame

dft <- as_tibble(df)
dft
dft$x ## error, no partial match done
dft[,'xyz'] ## column `xyz` returned as a tibble
dft[, c('abc', 'xyz')] ## returns both columns as a tibble

## mis-spelled columns of a data.frame may not result in errors and might return
## incorrect data because of partial matching
## column name is lost when accessing a column in data.frame

## 3.
col_name <- 'mpg'
mtcars_tibble <- as_tibble(mtcars)
mtcars_tibble[[col_name]]

## 4.
annoying <- tibble(
  `1` = 1:10,
  `2` = `1` * 2 + rnorm(length(`1`))
)

## 1)
annoying[['1']]

## 2)
annoying %>%
    ggplot(aes(x=`1`, y=`2`)) +
    geom_point()

## 3)
(annoying <- annoying %>%
     mutate(`3`=`2`/`1`))

## 4)
annoying %>%
    rename(one=`1`, two=`2`, three=`3`)

## 5.
?enframe

(greetings <- c('hello','goodbye','later'))
(eg <- enframe(greetings))
deframe(eg)

(ages <- c('bjarne'=31, 'rich'=22, 'martin'=54))
enframe(ages)
enframe(ages, name='name', value='age')

## we can use it when we have to convert a vector to a data frame/tibble

## 6.
## n_extra argument to print() function determines the number of extra columns
## to print information for.

## 11. Data Import
heights <- read_csv('data/heights.csv')

read_csv('The first line of metadata
  and the second line of metadata
  x,y,z
  1,2,3', skip=2)

read_csv('# a comment
  x,y,z
  1,2,3', comment='#')

read_csv('1,2,3\n4,5,6', col_name=FALSE)

read_csv('1,2,3\n4,5,6', col_name=c('x', 'y', 'z'))

read_csv('a,b,c\n1,2.1,.', na='.')
