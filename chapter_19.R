### Chapter 19

library(magrittr)

### practice 19.2.1
### 1.
rescale <- function(x) {
    rng <- range(x, na.rm = TRUE, finite = TRUE)
    (x - rng[1]) / (rng[2] - rng[1])
}
(x <- c(NA, 1:10, Inf, NA))
rescale(x)
### As long as `finite=TRUE` the value of na.rm doesn't matter as NA will
### be removed anyway

rescale <- function(x, rm) {
    rng <- range(x, na.rm = rm)
    (x - rng[1]) / (rng[2] - rng[1])
}
(x <- c(NA, 1:10))
rescale(x, FALSE)
### With out `finite` argument to range(), all values turn to NA if na.rm=FALSE

### 2.
rescale <- function(x) {
    rng <- range(x, na.rm = TRUE, finite = TRUE)
    x <- (x - rng[1]) / (rng[2] - rng[1])
    x[is.infinite(x) & x > 0] = 1
    x[is.infinite(x) & x < 0] = 0
    x
}
(x <- c(NA, 1:10, Inf, NA, -Inf))
rescale(x)
### 3.
proportion_na <- function(x) {
    mean(is.na(x))
}

proportions <- function(x, na.rm=FALSE) {
    x / sum(x, na.rm = na.rm)
}

rescale(x) <- function(x, na.rm=FALSE) {
    sd(x, na.rm = na.rm) / mean(x, na.rm = na.rm)
}

### 4.
### yeah, not gonna do that

### 5.
both_na <- function(v1, v2) {
    length(which(is.na(v1) & is.na(v2)))
}
both_na(c(1, NA, 2, 3, NA, 4), c(-9, NA, 12, 31, 22, 0))

### 6.
### the two functions give a meaningful name to the somewhat cryptic expressions
### we can make code more readable by using these function names

### 7.
### yeah, not gonna do that

### exercises 19.3.1
### 1.
?substr
?nchar

words <- c('hello', 'bye', 'world', 'universe')
nchar(words)
p = 'world'
substr('world is my oyster', 1, nchar(p)) == p

starts_with <- function(string, prefix) {
    substr(string, 1, nchar(prefix)) == prefix
}

words[-length(words)]
drop_last <- function(x) {
    if(length(x) <=1) return(NULL)
    x[-length(x)]
}
drop_last(words)

x <- c(4,2,5,1)
recycle <- function(x, y) {
    rep(y, length.out=length(x))
}
